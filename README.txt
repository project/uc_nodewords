
Description
-----------
This module integrates uc_catalog and nodewords modules, allowing
for Ubercart catalog pages to have their meta tags set by the 
term's meta tags


Installation
------------
1) Place this module directory in your modules folder (this will usually be
"sites/all/modules/").

2) Enable the module.


Author
------
Chris Burgess

* mail: chris@giantrobot.co.nz
* website: http://www.giantrobot.co.nz

The author can be contacted for paid customizations of this module as well as
Drupal consulting, development and installation.
